import java.util.HashMap;
import java.util.Map;

/**
 * Solution for Networks COMP2221 Coursework 2
 * problem task.
 * @author Sali Hyusein
 *
 * The implementation of the communication protocol between
 * the voting server and its multiple clients is defined below.
 * is defined below.
 *
 * Each server can respond to two type of requests from clients:
 * Either a query to show the current poll state OR a casted
 * vote for one of the available options. Clients can vote
 * multiple times. If a request does not match the above
 * described properties, a useful message is output, indicating
 * the type of issue found. */

public class VotingProtocol {
    // the types of request a client can perform
    private enum queryType {SHOW, VOTE, INVALID}
    // the actual request been made
    private queryType theType = null;

    /*
    * a key value pair dictionary, where each
    * key is an item that can be voted for and
    * the value represents the number of current
    * votes for that item */
    private HashMap<String, Integer> votes = null;
    // selected vote option (if present)
    private String voteOption = null;

    /*
    * When defining the protocol between the client and
    * the host, it is also adamant that the server specifies
    * items available to be voted for at any given time.
    * These are set as command line arguments when a server is
    * started. */
    public VotingProtocol(String[] voteItems) throws IllegalArgumentException {
        if (voteItems.length < 2) {
            throw new IllegalArgumentException(
                    "The server could not be started. "
                    + "There must be at least two voting options "
                    + "supplied as command line arguments."
            );
        }
        else {
            // initialises the dictionary
            votes = new HashMap<>();

            /*
            * for each argument item, creates a record in the
            * dictionary. There are initially no votes for any
            * of the items, hence also sets the respective
            * values to 0 */
            for (String item: voteItems) {
                votes.put(item, 0);
            }
        }
    }

    // Processes the input received from the server and returns
    // the appropriate response or error message.
    public String processInput(String input) {
        // the message generated if an error was encountered
        String errMessage = null;

        // the (normalised) request components should be split on space
        String[] inputParts = input.trim().toLowerCase().split("\\s+");

        /*
         * Verifiers for client's request made, based on
         * supplied cla. Each client can only query for the
         * current poll status with 'show' or vote for one
         * of the available options. */
        switch (inputParts.length) {
            // when no arguments are supplied.
            case 0:
                errMessage = "No request has been made\n"
                        + "Please supply one of the following: \n"
                        + "1) 'show' to see current poll status. \n"
                        + "2) 'vote <option>' to vote for one of the choices \n";
                theType = queryType.INVALID;
                break;
            // when a single component is supplied it must be a 'show' request.
            case 1:
                if (!inputParts[0].equals("show")) {
                    errMessage = "Invalid request made. \n"
                            + "Valid requests are of the form: \n"
                            + "'show' or 'vote <option>' \n";
                    theType = queryType.INVALID;
                }
                else {
                    theType = queryType.SHOW;
                }
                break;
            // when two components are supplied, a request should constitute of
            // the keyword 'vote' paired with a selected option.
            case 2:
                if (!inputParts[0].equals("vote")) {
                    errMessage = "Invalid request. \n";
                    theType = queryType.INVALID;
                }
                else {
                    boolean isValid = false;
                    // verifies that the option exists in the voting system.
                    for (Map.Entry<String, Integer> entry: votes.entrySet()) {
                        if (entry.getKey().equals(inputParts[1])) {
                            voteOption = inputParts[1];
                            isValid = true;
                            break;
                        }
                    }

                    theType = queryType.INVALID;
                    errMessage = inputParts[1] + " is not a valid vote option.";

                    // if all checks have passed, then it is a valid vote request.
                    if (isValid) {
                        theType = queryType.VOTE;
                    }
                }
                break;

            // Any request that contains more than 2 components is invalid.
            default:
                errMessage = "Invalid request. Too many arguments in request.\n";
                // when the user attempts to vote for more than one option at a time.
                if (inputParts[0].equals("vote")) {
                    errMessage = errMessage + "You can only vote for one option at a time\n";
                }
                theType = queryType.INVALID;
                break;
        }


        return determineOutput(errMessage);
    }

    /*
     * After the type of request has been determined from processing
     * the input, he appropriate call to the method that handles
     * the appropriate formatting has to invoked when a valid show
     * or vote request has been made. Otherwise the accumulated error
     * message from processing is returned. */
    private String determineOutput(String errMessage) {
        switch (theType) {
            case SHOW:
                return showRequestOut();
            case VOTE:
                return voteRequestOut();
            default:
                return errMessage;
        }
    }

    private String showRequestOut() {
        return currPollStatus();
    }

    // Updates the counter of the selected item in the voting system
    // and returns information about the newly updated poll status.
    private String voteRequestOut() {
        // increments the vote count of the selected item.
        votes.put(voteOption, votes.get(voteOption) + 1);
        String response =
                "Vote for "
                + voteOption
                + " recorded. New count: "
                + votes.get(voteOption);
        return response;
    }

    // Prints a line for each record of the dictionary, representing
    // the item and the number of votes it currently has.
    private String currPollStatus() {
        StringBuilder sb = new StringBuilder();
        sb.append("\n\nCurrent poll status:\n");
        // formats a line for each item in the voting system
        for (Map.Entry<String, Integer> entry: votes.entrySet()) {
            sb.append("'" + entry.getKey() + "' ");
            sb.append("has " + entry.getValue() + " vote(s).\n");
        }
        return sb.toString();
    }
}
