import java.net.*;
import java.io.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Solution for Networks COMP2221 Coursework 2
 * problem task.
 * @author Sali Hyusein
 *
 * The implementation of a server host is defined below.
 *
 * Each server is responsible for responding to multiple clients'
 * queries concurrently following a communication protocol defined in
 * VotingProtocol class. New thread of execution (up to 20) is created
 * each time a client connects to the server. Threads are handled by
 * the ClientHandler following the VotingProtocol definition.*/
public class Server
{
	// the socket connection of the server
	private ServerSocket serverSocket = null;
	// the executor multi-thread service
	private ExecutorService service = null;
	// the protocol of communication
	private volatile VotingProtocol votingProtocol = null;
	// the log file stream writer object.
	private volatile PrintWriter logWriter = null;
	// flag indicating that the server is listening
	boolean listening = true;

	public Server(String[] args) throws IOException{
		try {
			// the server starts listening on port 7777
			serverSocket = new ServerSocket(7777);
		}
		catch (IOException e) {
			System.err.println("Could not listen on port: 7777.");
			System.exit(1);
		}

		try {
			logWriter = new PrintWriter(new FileWriter("log.txt"));
		}
		catch (IOException e) {
			System.err.println("Could not initialise log file writer.");
			System.exit(1);
		}

		// initialises the protocol of communication
		votingProtocol = new VotingProtocol(args);

		// creates a new concurrent service, which accepts a
		// maximum of 20 threads at any given time.
		service = Executors.newFixedThreadPool(20);

		while (listening) {
			// waits until a client has connected to the server.
			Socket client = serverSocket.accept();
			// handles each client in a separate thread.
			service.submit(new ClientHandler(client, logWriter, votingProtocol));
		}

		// closes the socket the server listens to
		serverSocket.close();
		// closes the log file
		logWriter.close();
	}

	/*
	* Execution starts from the main method. The server is initialised
	* with vote options defined by the cla and it starts listening
	* for connections */
	public static void main( String[] args )
	{
		try {
			// creating an instance of server also initialises it
			new Server(args);
		}
		catch (IOException e) {
			System.out.println(e);
		}
	}
}