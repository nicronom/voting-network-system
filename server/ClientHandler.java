import java.net.*;
import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Solution for Networks COMP2221 Coursework 2
 * problem task.
 * @author Sali Hyusein
 *
 * The implementation of a client host handler is defined below.
 *
 * Each instantiation of this object represents a
 * new thread of execution, handling communication between
 * a querying client and a responding server.
 *
 * After each request response the connection to that client
 * is terminated and the reserved thread for that communication
 * becomes available. */

public class ClientHandler extends Thread{

    // the socket connection to a client
    private Socket clientSocket = null;
    /*
    * the protocol of communication is shared between
    * the server and the client handler, which handles
    * the one-to-one correspondence thread connection of
    * a client to the server. Volatile ensures both Server
    * and ClientHandler classes to have access to the same
    * communication protocol, which stores and updates the
    * dictionary counters. */
    private volatile VotingProtocol votingProtocol = null;
    /*
     * the stream writer to a request log file. A global
     * instance of this writer is passed to ClientHandler,
     * when a connection to client has been made. The server
     * itself does not perform writes to the log file, but
     * is responsible for closing it on server termination. */
    private volatile PrintWriter logWriter = null;
    // date formatter object is defined below
    private static final DateFormat df = new SimpleDateFormat("yyyy/MM/dd:HH/mm/ss");

    public ClientHandler(Socket socket,
                         PrintWriter log,
                         VotingProtocol protocol) {
        super();
        this.clientSocket = socket;

        // sets the thread reference to the global log file
        logWriter = log;

        // sets the protocol shared across all threads
        votingProtocol = protocol;
    }

    // The thread context is ran when the scheduler assigns CPU resources to it.
    public void run() {

        try {
            // in order to connect server's writes to client's reads,
            // similarly to the client auto flushing is enabled on println.
            PrintWriter out = new PrintWriter(
                    clientSocket.getOutputStream(), true
            );

            /*
             * conversely for the server's read stream. In addition,
             * the input received from the client socket is buffered
             * for efficieny. */
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(
                            clientSocket.getInputStream())
            );

            performRequest(in, out);

            // closes all connections made to that client
            // once the request has been served.
            out.close();
            in.close();
            clientSocket.close();
        }
        catch (IOException e) {
            // Any unexpected I/O error should be retraced and fixed.
            System.out.println(e);
        }
    }

    /*
    * The request to the server contains alterations to shared data
    * residing in the internal memory of voting protocol object.
    * Recording the request to the log file also needs to be synchronised,
    * since a request made later could end up executing earlier due to
    * non-deterministic execution of threads as selected by the scheduler. */
    private synchronized void performRequest(BufferedReader in,
                                             PrintWriter out) throws IOException {
        // the request input line received from the client
        String requestLine;
        // the response output line to be send to the client
        String responseLine;

        requestLine = in.readLine();
        logRequest(clientSocket, requestLine);
        responseLine = votingProtocol.processInput(requestLine);
        out.println(responseLine);
    }

    // Given a client socket performing the request, records the
    // date and time of the request made, the client ip address and
    // the actual request command in the format date:time:clientAddress:request
    private void logRequest(Socket clientSocket, String request) {
        // gets the current date and time and formats
        // it in a clearer way below.
        String date = df.format(new Date());

        // gets the address of the client issuing the request
        String address = clientSocket.getInetAddress().getHostAddress();
        // formats the log line appropriately
        logWriter.println(date + ":" + address + ":" + request);

        // data to log file has to be manually flushed in order to
        // not lose buffer and preserve the integrity of the
        // sequential writes to data.
        logWriter.flush();
    }
}
