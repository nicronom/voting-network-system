import java.io.*;
import java.net.*;

/**
 * Solution for Networks COMP2221 Coursework 2
 * problem task.
 * @author Sali Hyusein
 *
 * The implementation of a client host is defined below.
 *
 * The type of allowed requests is abstracted from the definition
 * of a client, as such the communication protocol is responsible
 * for handling any inapproriate requests made from the client user
 * and display an appropriate response describing the encountered error.
 *
 * The connection to the server is closed and all dynamic resources used
 * are released when all responds to the request made are received. */
public class Client
{

	// the socket connection of the client.
	private Socket clientSocket = null;
	// the output stream writer of the client.
	private PrintWriter socketOut = null;
	// the input stream reader of the client.
	private BufferedReader socketIn = null;
	// the message to be sent by the client
	private String clientMessage = null;

	public void performRequest(String[] request) {

		// formats all cla components into one string
		// in order to send a single request message to server.
		StringBuilder sb = new StringBuilder();
		for (String part: request) {
			sb.append(part);
			sb.append(' ');
		}
		clientMessage = sb.toString();

		try {
			// attempts to connect to the server listening on
			// localhost at port 7777.
			clientSocket = new Socket("localhost", 7777);

			// the socket's output stream with auto flush is chained below.
			// Buffer is flushed on println instead of '\n'.
			socketOut = new PrintWriter(clientSocket.getOutputStream(), true);

			// the input stream of socket is chained below.
			socketIn = new BufferedReader(
					new InputStreamReader(clientSocket.getInputStream()));

			// Write the formatted command line request to the server.
			socketOut.println(clientMessage);

			String serverResponse;
			// prints all received serve responses.
			while ((serverResponse = socketIn.readLine()) != null) {
				System.out.println(serverResponse);
			}
			// finally close all communication to return
			// all resources back to the OS.
			socketIn.close();
			socketOut.close();
			clientSocket.close();
		}
		catch (UnknownHostException e) {
			System.err.println("Could not connect to host. \n");
			System.exit(1);
		}
		catch (IOException e) {
			System.err.println("Could not connect with host IO. \n");
			System.exit(1);
		}
	}

	/*
	* Execution starts from the main method.
	* Client object is instantiated and the command line
	* arguments supplied form the client's request to be made. */
	public static void main( String[] args )
	{
		Client client = new Client();
		client.performRequest(args);
	}
}
